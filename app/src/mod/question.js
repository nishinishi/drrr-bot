
exports.default = class Question {
  constructor(bot) {
    this.bot = bot;
    this.on  = true;

    this.mem = {};
    this.pos = {
      'how many' : '1|2|3|4|5|None|Lots|A fuckton',
      'when'     : 'Tomorrow|Never|In a century|In a hundred million years',
      'why'      : 'Idk|Because youre silly|Because im in love with you|Because you touch yourself at night',
      'what'     : 'Idk|An elephant|A squirrel',
      'other'    : 'Yes|No|Maybe|Who knows|Not in your lifetime|Yes, but not to you|Fuck off|Stop talking to me|Blah blah and more blah...'
    };

    Object.keys(this.pos).forEach( key =>
      this.pos[key] = this.pos[key].split('|').map(_=>_+'.')
    );
  }

  random(key) {
    return this.pos[key][ Math.floor( Math.random() * this.pos[key].length ) ];
  }

  run(_q, talk) {
    const q   = _q.trim();
    const key = q+talk.uid;

    if(this.mem[key]) {
      this.bot.message(`I already said that ${this.mem[key].toLowerCase()}`);
    }
    else {
      if( q && q[q.length-1] === '?' ) {
        const pos = ['How many', 'when', 'why', 'other'];
        let res;

        for(let i = 0; i < pos.length; i++) {
          if( q.toLowerCase().includes( pos[i].toLowerCase() ) ) {
            res = this.random(pos[i]);
            break;
          }
        }

        this.mem[key] = res ? res : this.random('other');
      }
      else {
        this.mem[key] = 'That isn\'t even a question...';
      }
  
      this.bot.message(this.mem[key]);
    }
  }
}

